package com.liferay.tests.invoice.controller;

import java.io.File;
import java.io.IOException;
import java.util.EnumSet;
import org.junit.Test;
import com.liferay.tests.invoice.util.FileReaderTest.TEST_INPUT;

/**
 *
 * @author boris.baldovino
 */
public class InvoiceControllerTest {
    
    /**
     * The controller reference.
     */
    InvoiceController invoiceController = InvoiceController.INSTANCE;

    /**
     *
     * @throws IOException
     */
    @Test
    public void testPrintInvoice() throws IOException {
        for (TEST_INPUT testInput : EnumSet.of(TEST_INPUT.INPUT_1, TEST_INPUT.INPUT_2, TEST_INPUT.INPUT_3)) {
            File invoiceFile = testInput.file();
            invoiceController.printInvoice(invoiceFile);
        }
    }
}

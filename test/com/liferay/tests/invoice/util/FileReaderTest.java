package com.liferay.tests.invoice.util;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.junit.Test;

/**
 *
 * @author boris.baldovino
 */
public class FileReaderTest {
    
    private static File inputFileDirectory;
    static {
        try {
            inputFileDirectory = new File(new File(".").getCanonicalFile(), "input_files");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static enum TEST_INPUT {
        INPUT_1, INPUT_2, INPUT_3;
        
        /**
         * @return File for reading.
         */
        public File file() {
            return new File(inputFileDirectory, this.toString() + ".txt");
        }
    }

    @Test
    public void readRegularInput() throws IOException {
        
        final BlockingQueue<String> lines = new LinkedBlockingQueue<String>();
        File input = TEST_INPUT.INPUT_2.file();
        FileReader.INSTANCE.getFile(input).read();
        
        System.out.println(lines);
    }

}

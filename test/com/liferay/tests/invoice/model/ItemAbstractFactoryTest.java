package com.liferay.tests.invoice.model;

import java.util.EnumSet;
import java.util.Set;
import junit.framework.Assert;
import org.junit.Test;

/**
 * Unit test for Item Abstract Factory 
 * @author boris.baldovino
 */
public class ItemAbstractFactoryTest {
    
    private void assertItemParsedCorrectly(int testedQuantity, String testedDescription, float testedPrice, 
            Set<TaxRate> testedTaxRates, Item item) {
        Assert.assertEquals("Wrong quantity.", item.getQuantity(), testedQuantity);
        Assert.assertEquals("Wrong description. ", item.getDescription(), testedDescription);
        Assert.assertEquals("Wrong price. ", item.getPrice(), testedPrice);
        Assert.assertTrue("Wrong tax(es). ", testedTaxRates.containsAll(item.getTaxes()));
    }

    @Test
    public void parsingRegularItems() {
        Item item = ItemAbstractFactory.INSTANCE.create("1 bottle of perfume at 18.99");
        Set<TaxRate> expectedTaxes = EnumSet.of(TaxRate.REGULAR);
        assertItemParsedCorrectly(1, "bottle of perfume", 18.99f, expectedTaxes, item);
    }
    
    @Test
    public void parsingImportedItems() {
        Item item = ItemAbstractFactory.INSTANCE.create("1 imported bottle of perfume at 47.50");
        Set<TaxRate> expectedTaxes = EnumSet.of(TaxRate.IMPORTED, TaxRate.REGULAR);
        assertItemParsedCorrectly(1, "imported bottle of perfume", 47.5f, expectedTaxes, item);
    }

    @Test
    public void parsingExemptImportedItems() {
        Item item = ItemAbstractFactory.INSTANCE.create("27 box of imported chocolates at 11.25");
        Set<TaxRate> expectedTaxes = EnumSet.of(TaxRate.TAX_EXEMPT, TaxRate.IMPORTED);
        assertItemParsedCorrectly(27, "box of imported chocolates", 11.25f, expectedTaxes, item);
    }

    @Test
    public void parsingExemptItems() {
        Item item = ItemAbstractFactory.INSTANCE.create("1 chocolate bar at 0.85");
        Set<TaxRate> expectedTaxes = EnumSet.of(TaxRate.TAX_EXEMPT);
        assertItemParsedCorrectly(1, "chocolate bar", 0.85f, expectedTaxes, item);
    }
    
}

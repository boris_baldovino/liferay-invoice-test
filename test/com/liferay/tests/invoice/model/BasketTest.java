package com.liferay.tests.invoice.model;

import java.io.File;
import java.io.IOException;
import java.util.EnumSet;
import java.util.List;
import org.junit.Test;
import com.liferay.tests.invoice.util.FileReader;
import com.liferay.tests.invoice.util.FileReaderTest.TEST_INPUT;

/**
 * Unit tests for Basket of Items.
 * @author boris.baldovino
 */
public class BasketTest {
    
    @Test
    public void testReadingInputsEntries() throws IOException {
        
        for (TEST_INPUT testInput : EnumSet.of(TEST_INPUT.INPUT_1, TEST_INPUT.INPUT_2, TEST_INPUT.INPUT_3)) {
            File fileInput = testInput.file();
            List<String> itemEntries = FileReader.INSTANCE.getFile(fileInput).read();
            itemEntries.clear();
        }
    }
}

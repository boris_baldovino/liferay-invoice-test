Solution for the coding problem (THE_CODING_PROBLEM.txt):

I used Object-Oriented Analysis, design patterns and the implementation of the software pattern MVC (Model View Controller) to came with this solution. I used Netbeans IDE 8.0, JDK 1.6, and the solution also have a complete documentation (../dist/javadoc), and TDD classes (Test-Driven Development with JUnit 4).

I started with Test-Driven Development for the input file reader classes (FileReader.java and FileReaderTest.java). The test class read the input files from the ../input_files directory and shows the results by console. Both classes receive by parameter the location of the input file to be process.

The model has the following considerations:
- An Item object is a product with a <price> and <description> and <quantity>.
- A Basket object is a set of Items. (Basically, it's a row of the invoice).
- The TaxRate object was implement to manage the regular, imported and exempt taxes.

For the actual implementation was created the InvoiceController Enum Singleton (also the InvoiceControllerTest class for testing). Also "Operations" enum singletons where included:
- The InvoiceReader for reading the input file (calling the FileReader enum) and represent the invoice objects from the model.
- A decorator pattern was used to represent the objects in the InvoiceDecorator class.
- For calculate the taxes (based on the TaxRate enum) and total costs, was implemented the InvoiceCompute.java.

The view was implemented in the ConsoleView.java.

Finally the application can be executed with the InvoiceApplication class. The .jar generated is placed in the ../dist directory an can be call by console and receive as parameter the input file path:

java -jar "Invoice.jar" ../input_files/input_1.txt
package com.liferay.tests.invoice.controller;

import java.io.File;
import java.io.IOException;
import com.liferay.tests.invoice.model.Item;
import com.liferay.tests.invoice.model.Basket;
import com.liferay.tests.invoice.operations.ItemDecorator;
import com.liferay.tests.invoice.operations.InvoiceReader;
import com.liferay.tests.invoice.view.ConsoleView;

/**
 * Invoice controller used to manage and validate different scenarios.
 * 
 * @author boris.baldovino
 *
 */
public enum InvoiceController {

    /**
     * Singleton instance.
     */
    INSTANCE;
    
    /**
     * Invoice reader.
     */
    private final InvoiceReader invoiceReader = InvoiceReader.INSTANCE;

    /**
     * Prints the invoice.
     * @param inputFile
     * @throws java.io.IOException
     */
    public void printInvoice(File inputFile) throws IOException {
        Basket<Item> basket = invoiceReader.loadBasket(inputFile);
        ItemDecorator decorator = ItemDecorator.INSTANCE;
        ConsoleView.INSTANCE.render(basket, decorator);
    }
}

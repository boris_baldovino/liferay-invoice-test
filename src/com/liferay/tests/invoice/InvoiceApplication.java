package com.liferay.tests.invoice;

import java.io.File;
import java.io.IOException;
import com.liferay.tests.invoice.controller.InvoiceController;

/**
 *
 * @author boris.baldovino
 */
public class InvoiceApplication {
    
    private static enum Error {

        NO_ARGS_GIVEN(0xEAFA, "No given args. The path of the input file is missed."),
        NOT_A_FILE(0xC0EF, "The path given is not a file.");

        private final int code;
        private final String description;

        private Error(int code, String description) {
            this.code = code;
            this.description = description;
        }

        @Override
        public String toString() {
            return "Error " + this.code + ": " + this.description;
        }
    }

    /**
     * If the given arguments don't contain a valid input file, 
     * the application will show an error message and exit.
     * 
     * @param args path of the input file.
     * @return File
     */
    public static File getInputFile(String ... args) {
        if (args.length == 0) {
            Error error = Error.NO_ARGS_GIVEN;
            System.err.println(error);
            System.exit(error.code);
        }
        File invoiceFile = new File(args[0]);
        if (!invoiceFile.isFile()) {
            Error error = Error.NOT_A_FILE;
            System.err.println(error);
            System.exit(error.code);
        }
        return invoiceFile;
    }

    /**
     * Main method executed by line command 
     * @param args
     */
    public static void main(String[] args) {
        // Get input file from arguments.
        File invoiceFile = getInputFile(args);

        // Once the file is valid.
        InvoiceController invoiceController = InvoiceController.INSTANCE;

        // Print invoice.
        try {
            invoiceController.printInvoice(invoiceFile);

        } catch (IOException e) {
            System.err.println(e.getMessage());
            System.exit(0);
        }
    }
}

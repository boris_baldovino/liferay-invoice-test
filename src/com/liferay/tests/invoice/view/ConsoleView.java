package com.liferay.tests.invoice.view;

import com.liferay.tests.invoice.model.Item;
import com.liferay.tests.invoice.model.Basket;
import com.liferay.tests.invoice.operations.ItemDecorator;
import com.liferay.tests.invoice.operations.InvoiceCompute;

/**
 * Console view of the output invoice.
 * 
 * @author boris.baldovino
 *
 */
public enum ConsoleView implements InvoiceView {
    /**
     * Singleton instance.
     */
    INSTANCE;
    
    /**
     * Constant message for taxes.
     */
    private static final String TAXES_MESSAGE = "Sales Taxes: ";
    /**
     * Constant message for total.
     */
    private static final String TOTAL_MESSAGE = "Total: ";
    /**
     * The instance of the calculator service.
     */
    private final InvoiceCompute compute = InvoiceCompute.INSTANCE;

    @Override
    public void render(Basket<Item> basket, ItemDecorator decorator) {
        for (Item item : basket) {
            System.out.println(decorator.decorate(item, compute));
        }

        System.out.print(TAXES_MESSAGE);
        System.out.print(compute.totalTaxes(basket) + "");
        
        System.out.println("");
        System.out.print(TOTAL_MESSAGE);
        System.out.print(compute.basketTotalCost(basket));
    }
}

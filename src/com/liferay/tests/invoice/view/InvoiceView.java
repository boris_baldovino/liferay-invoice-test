package com.liferay.tests.invoice.view;

import com.liferay.tests.invoice.model.Item;
import com.liferay.tests.invoice.model.Basket;
import com.liferay.tests.invoice.operations.ItemDecorator;

/**
 * Renders the Basket.
 * 
 * @author boris.baldovino
 *
 */
public interface InvoiceView {

    /**
     * Render a Basket.
     * @param basket Basket of Items.
     * @param decorator For decorate the items.
     */
    public void render(Basket<Item> basket, ItemDecorator decorator);
    
}

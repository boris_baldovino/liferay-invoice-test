package com.liferay.tests.invoice.util;

import java.io.BufferedReader;
import java.io.CharArrayReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.LinkedList;
import java.util.List;

/**
 * Reads a file.
 * 
 * @author boris.baldovino
 * 
 */
public enum FileReader {
    /**
     * Singleton instance.
     */
    INSTANCE;

    /**
     * File for reading.
     */
    private File file;

    /**
     * Gets the file from the instance.
     * @param file File for reading.
     * @return This instance.
     */
    public FileReader getFile(File file) {
        this.file = file;
        return this;
    }

    /**
     * Reads the entire file.
     * @return List of lines taken from the input file.
     * @throws IOException
     */
    public List<String> read() throws IOException {
        List<String> invoiceEntries = new LinkedList<String>();
        FileInputStream inFile = new FileInputStream(this.file);
        FileChannel inChannel = inFile.getChannel();
        ByteBuffer byteBuffer = ByteBuffer.allocate((int) inChannel.size());
        final Charset charSet = Charset.forName("ASCII");

        while (inChannel.read(byteBuffer) != -1) {
            byteBuffer.flip();
            CharBuffer charBuffer = charSet.decode(byteBuffer);
            BufferedReader br = new BufferedReader(new CharArrayReader(charBuffer.array()));
            String line;
            while ((line = br.readLine()) != null) {
                line = line.trim();
                invoiceEntries.add(line);
            }
            byteBuffer.clear();
        }
        inFile.close();
        return invoiceEntries;
    }
}

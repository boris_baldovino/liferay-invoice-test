package com.liferay.tests.invoice.operations;

import com.liferay.tests.invoice.model.Item;

/**
 * Decorate the entries of the output.
 * @author boris.baldovino
 */
public enum ItemDecorator {
    /**
     * Singleton instance.
     */
    INSTANCE;
    
    /**
     * Prints the invoice.
     * @param item
     * @param compute
     * @return Decorated Entry with the output format.
     */
    public String decorate(Item item, InvoiceCompute compute) {
        StringBuilder decoratedEntry = new StringBuilder();

        // Quantity, description and total cost entry.
        decoratedEntry.append(item.getQuantity());
        decoratedEntry.append(" ");

        decoratedEntry.append(item.getDescription());

        decoratedEntry.append(": ");
        decoratedEntry.append(compute.totalCost(item));
        
        return decoratedEntry.toString();
    }
}
package com.liferay.tests.invoice.operations;

import java.io.File;
import java.io.IOException;
import java.util.List;
import com.liferay.tests.invoice.model.ItemAbstractFactory;
import com.liferay.tests.invoice.model.Item;
import com.liferay.tests.invoice.model.Basket;
import com.liferay.tests.invoice.util.FileReader;

/**
 * Read from the input file for represent the data into items and basket objects.
 * 
 * @author boris.baldovino
 *
 */
public enum InvoiceReader {
    /**
     * Singleton instance.
     */
    INSTANCE;

    /**
     * Read the input file, create items from entries which are later added into a basket.
     * @param invoiceFile Input file.
     * @return Basket of Item.
     * @throws IOException For IO problems with the file.
     */
    public Basket<Item> loadBasket(File invoiceFile) throws IOException {
        Basket<Item> basket = Basket.newInstance();

        // Read entries from the input file and put them into a list.
        List<String> entryItems = FileReader.INSTANCE.getFile(invoiceFile).read();

        for (String entry : entryItems) {
            // Create an item for every entry found in the file and later is added into the basket.
            Item item = ItemAbstractFactory.INSTANCE.create(entry);
            basket.add(item);
        }

        return basket;
    }
}

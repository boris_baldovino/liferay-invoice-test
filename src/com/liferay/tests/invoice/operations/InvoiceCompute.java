package com.liferay.tests.invoice.operations;

import com.liferay.tests.invoice.model.Item;
import com.liferay.tests.invoice.model.TaxRate;
import com.liferay.tests.invoice.model.Basket;

/**
 * Different operations for calculate taxes rates and total costs.
 * @author boris.baldovino
 */
public enum InvoiceCompute {
    /**
     * Singleton instance.
     */
    INSTANCE;

    /**
     * @param item Single item.
     * @return Total price of an item without taxes.
     */
    public float totalPrice(Item item) {
        return item.getQuantity() * item.getPrice();
    }

    /**
     * @param item Single item.
     * @return Item taxes value.
     */
    public float itemTaxes(Item item) {
        float taxes = 0;
        float totalPrice = totalPrice(item);
        for (TaxRate taxRate : item.getTaxes()) {
            taxes += totalPrice * taxRate.getTaxRate();
        }
        return (float)Math.ceil(taxes / .05f) * .05f;
    }

    /**
     * @param item Single item.
     * @return Total cost of an item with taxes included.
     */
    public float totalCost(Item item) {
        return totalPrice(item) + itemTaxes(item);
    }
    
    /**
     * @param basket Basket of items.
     * @return Total cost of a basket of items with taxes included.
     */
    public float basketTotalCost(Basket<Item> basket) {
        float totalCost = 0;
        for (Item item : basket) {
            totalCost += totalCost(item);
        }
        return totalCost;
    }

    /**
     * @param basket Basket of items.
     * @return Total of taxes of a basket of items.
     */
    public float totalTaxes(Basket<Item> basket) {
        float totalTax = 0;
        for (Item item : basket) {
            totalTax += itemTaxes(item);
        }
        return (float)Math.ceil(totalTax * 100) / 100;
    }
}
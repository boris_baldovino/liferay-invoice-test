package com.liferay.tests.invoice.model;

/**
 * Applicable tax rates.
 * 
 * @author boris.baldovino
 *
 */
public enum TaxRate {

    /**
     * All items has a 10% tax rate.
     */
    REGULAR(.10f),
    /**
     * Books, food, and medical products are exempt from taxes.
     */
    TAX_EXEMPT(.0f),
    /**
     *  Import duty is an additional 5% sales tax, applicable on all imported items.
     */
    IMPORTED(.05f);

    private final float rate;

    private TaxRate(float rate) {
        this.rate = rate;
    }

    /**
     * Get the tax rate.
     * @return Rate.
     */
    public float getTaxRate() {
        return rate;
    }
}

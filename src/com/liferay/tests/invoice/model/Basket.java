package com.liferay.tests.invoice.model;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Basket represents a set of Item.
 * @author boris.baldovino
 * @param <T>
 */
public class Basket<T extends Item> implements Iterable<T> {
    /**
     * Set of items.
     */
    private final Set<T> items;

    private Basket() {
        this.items = new LinkedHashSet<T>();
    }
    
    /**
     * @param <T>
     * @return Basket without items.
     */
    public static <T extends Item> Basket<T> newInstance() {
        return new Basket<T>();
    }
    
    /**
     * Adds an item to the basket.
     * @param item Single item.
     */
    public void add(T item) {
        this.items.add(item);
    }

    @Override
    public Iterator<T> iterator() {
        Iterator<T> it = new Iterator<T>() {

            private final Iterator<T> iterator = items.iterator();

            @Override
            public boolean hasNext() {
                return iterator.hasNext();
            }

            @Override
            public T next() {
                return iterator.next();
            }

            @Override
            public void remove() {
                throw new IllegalAccessError("Removed function is not implemented.");
            }
        };
        return it;
    }
}

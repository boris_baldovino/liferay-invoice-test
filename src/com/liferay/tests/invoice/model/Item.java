package com.liferay.tests.invoice.model;

import java.util.Set;

/**
 * Item represents an entry of the input file with quantity, description of the item and the single price.
 * @author boris.baldovino
 */
public interface Item {
    /**
     * @return Quantity of items.
     */
    int getQuantity();
    /**
     * @return Description of an item.
     */
    String getDescription();
    /**
     * @return Single price of an item.
     */
    float getPrice();
    /**
     * @return Tax rates of an item.
     * @see TaxRate
     */
    Set<TaxRate> getTaxes();
}

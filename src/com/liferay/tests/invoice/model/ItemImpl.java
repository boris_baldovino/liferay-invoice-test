package com.liferay.tests.invoice.model;

import java.util.Set;

/**
 * Represents an entry of the input file.
 * @author boris.baldovino
 */
public class ItemImpl implements Item {
    /**
     * Quantity of items.
     */
    private final int quantity;
    /**
     * Description of items.
     */
    private final String description;
    /**
     * Price of an item.
     */
    private final float price;
    /**
     * Set of taxes to the items of a line.
     */
    private final Set<TaxRate> taxes;

    /**
     * Creates a new Item entry with the given values.
     * @param quantity Quantity of items.
     * @param description Description of the item.
     * @param price Price of the item.
     * @param taxes Tax rates of an item.
     */
    public ItemImpl(int quantity, String description, float price, Set<TaxRate> taxes) {
        this.description = description;
        this.price = price;
        this.quantity = quantity;
        this.taxes = taxes;
    }

    @Override
    public final int getQuantity() {
        return this.quantity;
    }
    
    @Override
    public final String getDescription() {
        return this.description;
    }

    @Override
    public final float getPrice() {
        return this.price;
    }

    @Override
    public final Set<TaxRate> getTaxes() {
        return this.taxes;
    }

}

package com.liferay.tests.invoice.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

/**
 * Abstract Factory for creating items from the input file.
 * 
 * @author boris.baldovino
 */
public enum ItemAbstractFactory {

    /**
     * Singleton instance.
     */
    INSTANCE;
    
    /**
     * Key word for imported items.
     */
    private static final String IMPORTED_ITEMS = "imported";
    /**
     * Item with the exempt tax.
     */
    private static final List<String> EXEMPT_ITEMS = new ArrayList<String>(
        Arrays.asList("book","chocolate","pills"));
    
    /**
     * Create an item instance.
     * @param itemLine Line item from the input file.
     * @return An Item.
     */
    public Item create(String itemLine) {
        
        Scanner itemScanner = new Scanner(itemLine);
        
        // Looking for quantity.
        int quantity = -1;
        if (itemScanner.hasNextInt()) {
            quantity = itemScanner.nextInt();
        } else {
            throw new IllegalStateException("The itemLine '" + itemLine + "' don't start with a quantity.");
        }

        // Looking for description.
        StringBuilder builder = new StringBuilder();
        while (itemScanner.hasNext()) {
            String description = itemScanner.next();
            if (description.equals("at")) {
                break;
            }
            builder.append(description).append(" ");
        }
        String itemDescription = builder.toString().trim();

        // Looking for price.
        float price = -1f;
        if (itemScanner.hasNext()) {
            String floatToken = itemScanner.next();
            price = Float.valueOf(floatToken);
        }

        // Adding a TaxRate for all the items.
        Set<TaxRate> multiTaxValue = new HashSet<TaxRate>();
        multiTaxValue.add(TaxRate.REGULAR);

        // If the item is exempt from taxes, the regular tax is removed.
        for (String exemptToken : EXEMPT_ITEMS) {
            if (itemDescription.toLowerCase().contains(exemptToken)) {
                multiTaxValue.remove(TaxRate.REGULAR);
                multiTaxValue.add(TaxRate.TAX_EXEMPT);
                break;
            }
        }

        // Adding the imported rate, if the item is imported.
        if (itemDescription.toLowerCase().contains(IMPORTED_ITEMS)) {
            multiTaxValue.add(TaxRate.IMPORTED);
        }
        return new ItemImpl(quantity, itemDescription, price, multiTaxValue);
    }
}
